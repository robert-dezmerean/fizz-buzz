
def divisible_by_three(number):
    return divisible_by(3, number)


def divisible_by_five(number):
    return divisible_by(5, number)


def divisible_by_fifteen(number):
    return divisible_by(15, number)


def divisible_by(divisor, number):
    return number % divisor == 0


def fizzbuzz(number):
    if divisible_by_fifteen(number):
        return "FizzBuzz"
    if divisible_by_three(number):
        return "Fizz"
    if divisible_by_five(number):
        return "Buzz"
    else:
        return number


def play(number):
    if number > 0:
        play(number - 1)
        print(fizzbuzz(number))


if __name__ == "__main__":
    play(100)
