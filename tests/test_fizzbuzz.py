import unittest

import app.fizzbuzz as fizzbuzz


class TestDivisibleByThree(unittest.TestCase):

    def test_divisible_by_three(self):
        result = fizzbuzz.divisible_by_three(3)
        self.assertTrue(result)

    def test_not_divisible_by_three(self):
        result = fizzbuzz.divisible_by_three(1)
        self.assertFalse(result)


class TestDivisibleByFive(unittest.TestCase):

    def test_divisible_by_five(self):
        result = fizzbuzz.divisible_by_five(5)
        self.assertTrue(result)

    def test_not_divisible_by_five(self):
        result = fizzbuzz.divisible_by_five(1)
        self.assertFalse(result)


class TestDivisibleByFifteen(unittest.TestCase):

    def test_divisible_by_fifteen(self):
        result = fizzbuzz.divisible_by_fifteen(15)
        self.assertTrue(result)

    def test_not_divisible_by_fifteen(self):
        result = fizzbuzz.divisible_by_fifteen(1)
        self.assertFalse(result)


class TestFizzBuzz(unittest.TestCase):

    def test_playing_says_fiss(self):
        result = fizzbuzz.fizzbuzz(3)
        self.assertEqual(result, "Fizz")

    def test_playing_says_buss(self):
        result = fizzbuzz.fizzbuzz(5)
        self.assertEqual(result, "Buzz")

    def test_playing_says_fizzbuzz(self):
        result = fizzbuzz.fizzbuzz(15)
        self.assertEqual(result, "FizzBuzz")

    def test_playing_says_number(self):
        result = fizzbuzz.fizzbuzz(1)
        self.assertEqual(result, 1)
